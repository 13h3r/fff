organization := "ru.dgis.casino"

name := "bridge"

scalaVersion := "2.10.3"

version := "0.2.1-SNAPSHOT"

libraryDependencies ++= Seq(
  "org.scalaz" %% "scalaz-core" % "7.0.4",
  "org.postgresql" % "postgresql" % "9.2-1003-jdbc4",
  "com.typesafe.akka" %% "akka-actor" % "2.2.1",
  "com.typesafe.akka" %% "akka-remote" % "2.2.1",
  "com.typesafe.slick" %% "slick" % "1.0.1"
)

libraryDependencies ++= Seq(
  "org.scalatest" %% "scalatest" % "2.0.RC2" % "test",
  "com.typesafe.akka" %% "akka-testkit" % "2.2.1"
)

publishTo := Some(Resolver.url("sbt-plugin-releases", new URL("http://artifactory.billing.test:8081/artifactory/casino2/"))(Resolver.mavenStylePatterns))



