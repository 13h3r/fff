package ru.dgis.casino.bridge.actor

import akka.actor.Actor
import scala.xml.{Node, XML}
import scalaz.Scalaz._

case class AdsXmlItem(
  filialId: Long,
  filialGroupId: Long,
  priority: Int,
  project: String
)


object AdsXmlParser {

  case class ParseTask(url: String)

  case class ParserResponse(items: Iterable[AdsXmlItem], errors: Iterable[String])

}




class AdsXmlParser extends Actor {

  import AdsXmlParser._

  def receive = {
    case ParseTask(url) =>
      val parsingResult = XML.load(url) \ "adslist" \ "ads" map parseNode
      val (ok, err) = parsingResult.partition(_.isRight)
      sender ! ParserResponse(ok.map(_.right.get), err.map(_.left.get))

  }

  def parseNode(ads: Node) = {
    val filialIdEither = (ads \ "@filial_id").text.parseLong.toEither
    val filialGroupIdEither = (ads \ "@filial_group_id").text.parseLong.toEither
    val priorityEither = (ads \ "@priority").text.parseInt.toEither
    val projectIdEither = Either.cond(!(ads \ "@project").text.isEmpty, (ads \ "@project").text, new Exception("Project should not be empty") )

    val result = for {
      filialId <- filialIdEither.right
      filialGroupId <- filialGroupIdEither.right
      priority <- priorityEither.right
      projectId <- projectIdEither.right
    } yield AdsXmlItem(filialId, filialGroupId, priority, projectId)

    result.left.map(e => s"Unable to parse ads for filial_id = ${ads \ "@filial_id"}. Exception is '${e.getMessage}'")
  }
}
