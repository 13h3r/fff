package ru.dgis.casino.bridge.actor

import akka.testkit.TestActorRef
import scala.util.Success
import ru.dgis.casino.UnitTest


class AdsXmlParserTest extends UnitTest  {


  def predef = new {
    val actorRef = TestActorRef[AdsXmlParser]
    val actor = actorRef.underlyingActor

  }

  "Parser.parseNode()" should "parse correct xml" in {
    val p = predef
    p.actor.parseNode(
      <ads filial_id="141265769428570" filial_group_id="141274359270660" priority="2" project="novosibirsk">
        <block type="lnu"><![CDATA[http://www.ecoteka.ru/?utm_source=2gisOn]]></block>
        <block type="lna"><![CDATA[Интернет-магазин]]></block>
        <block type="fas"><![CDATA[ООО «АКВАДАН», 630559, Новосибирская обл., Новосибирский р-н, р.п. Кольцово, 22-27, ОГРН 1135476091640]]></block>
        <block type="mco"><![CDATA[Покупайте фильтры для воды у официального дилера по низким ценам!]]></block>
        <block type="obt"><![CDATA[Интернет-магазин экотехники]]></block>
        <block type="obb"><![CDATA[Увлажнители, воздухоочистители. Фильтры для воды. Доставка, самовывоз]]></block>
        <block type="inf"><![CDATA[Покупайте фильтры для воды у официального дилера по низким ценам!]]></block>
        <bargain>
          <title><![CDATA[С купоном 2ГИС фильтр обойдется вам на 5% дешевле!]]></title>
          <description><![CDATA[]]></description>
          <conditions><![CDATA[Cкидка предоставляется в любом отделе компании на все модели водоочистителей, не участвующие в какой-либо другой акции. Какой из фильтров лучше всего подойдет именно вам — подскажут продавцы. Спрашивайте, не стесняйтесь!]]></conditions>
          <promocode></promocode>
          <from>2013-08-31T17:00:00Z</from>
          <to>2013-12-30T17:00:00Z</to>
          <fas><![CDATA[ООО «Экопром»; 630025, г. Новосибирск, Бердское шоссе, 61а; ОГРН 1095473010522.]]></fas>
        </bargain>
      </ads>).right.get should equal (AdsXmlItem(141265769428570L, 141274359270660L, 2, "novosibirsk"))
  }

  it should "report error when filial id, group id or priority is not a numbers" in {
    val p = predef
    p.actor.parseNode(
        <ads filial_id="xxxx" filial_group_id="141274359270660" priority="2" project="novosibirsk"/>
    ) should be ('isLeft)

    p.actor.parseNode(
        <ads filial_id="141265769428570" filial_group_id="aaaa" priority="2" project="novosibirsk"/>
    ) should be ('isLeft)

    p.actor.parseNode(
        <ads filial_id="141265769428570" filial_group_id="141274359270660" priority="aaaaa" project="novosibirsk"/>
    ) should be ('isLeft)

  }

  it should "report error if one of the parameters are missed" in {
    val p = predef
    p.actor.parseNode(
        <ads filial_group_id="141274359270660" priority="2" project="novosibirsk"/>
    ) should be ('isLeft)

    p.actor.parseNode(
        <ads filial_id="141265769428570" priority="2" project="novosibirsk"/>
    ) should be ('isLeft)

    p.actor.parseNode(
        <ads filial_id="141265769428570" filial_group_id="141274359270660" project="novosibirsk"/>
    ) should be ('isLeft)

    p.actor.parseNode(
        <ads filial_id="141265769428570" filial_group_id="141274359270660" priority="2"/>
    ) should be ('isLeft)
  }

  it should "report error if ads tag is blank" in {
    val p = predef
    p.actor.parseNode(
        <ads/>
    ) should be ('isLeft)
  }

  "Parser as actor" should "handle parse message and return parsed info" in {
    import akka.pattern.ask

    val p = predef
    val file = this.getClass.getResource("parser-test.xml")
    val future = p.actorRef ? AdsXmlParser.ParseTask(file.toString)
    val Success(result : AdsXmlParser.ParserResponse) = future.value.get
    result.items should have size 2
    result.items should contain (AdsXmlItem(1L, 1L, 1, "1"))
    result.items should contain (AdsXmlItem(2L, 2L, 2, "2"))
    result.errors should have size 2
  }




}
