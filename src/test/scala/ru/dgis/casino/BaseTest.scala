package ru.dgis.casino

import org.scalatest.{Matchers, FlatSpec}
import akka.actor.ActorSystem
import akka.util.Timeout

abstract class UnitTest extends FlatSpec with Matchers with ActorTest

trait ActorTest {
  implicit lazy val system = ActorSystem()
  implicit lazy val timeout = Timeout(1)

}
